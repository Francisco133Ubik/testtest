//
//  ViewController.swift
//  testtest
//
//  Created by Francisco on 01/11/22.
//

import UIKit

class ViewController: UIViewController {
    let button = UIButton()

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    private func setup() {
        view.addSubview(button)
        button.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            button.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            button.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor,
                                           constant: -20),
            button.heightAnchor.constraint(equalToConstant: 44),
            button.widthAnchor.constraint(equalToConstant: 100)
        ])
        button.setTitle("Tap", for: .normal)
        button.backgroundColor = .systemGreen
        button.addTarget(self, action: #selector(tap), for: .touchUpInside)
    }

    @objc func tap() {
        present(DiagramViewController(), animated: true)
    }

}

