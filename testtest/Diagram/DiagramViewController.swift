//
//  DiagramViewController.swift
//  testtest
//
//  Created by Francisco on 01/11/22.
//

import UIKit

class DiagramViewController: UIViewController {
    let scrollView = UIScrollView()
    let contentView = UIView()
    let diagramView = DiagramView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupScrollView()
        setupDiagramView()
    }
    
    
    private func setupScrollView() {
        view.backgroundColor = UIColor.systemBackground
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        contentView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(scrollView)
        scrollView.addSubview(contentView)
        
        scrollView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        scrollView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        scrollView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        scrollView.bounces = false
    
        
        contentView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor).isActive = true
        contentView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor).isActive = true
        contentView.topAnchor.constraint(equalTo: scrollView.topAnchor).isActive = true
        contentView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor).isActive = true
    
    }
    
    private func setupDiagramView() {
        contentView.addSubview(diagramView)
        diagramView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            diagramView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            diagramView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            diagramView.topAnchor.constraint(equalTo: contentView.topAnchor),
            diagramView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
        ])
    }
    
}
