//
//  MatchView.swift
//  testtest
//
//  Created by Francisco on 01/11/22.
//

import UIKit

class MatchView: UIView {
    
    public enum Direction {
        case top
        case bottom
    }
    
    let shapeLayer = CAShapeLayer()
    let width = 120
    let height = 250

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    private func setupView() {
        backgroundColor = .green
        clipsToBounds = false
    }
    
    public func createPath(heightLine: Int, direction: Direction) {
        shapeLayer.path = createBezierPath(heightLine: heightLine,
                                           direction: direction).cgPath
        shapeLayer.strokeColor = UIColor.gray.cgColor
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.lineWidth = 4
        layer.addSublayer(shapeLayer)
    }
    
    private func createBezierPath(heightLine: Int, direction: Direction) -> UIBezierPath {
        let path = UIBezierPath()
        path.move(to: CGPoint(x: width,
                              y: height / 2))
        path.addLine(to: CGPoint(x: width + 100, y: height / 2))
        switch direction {
        case .top:
            path.addLine(to: CGPoint(x: width + 100, y: (height / 2) - heightLine ))
        case .bottom:
            path.addLine(to: CGPoint(x: width + 100, y: (height / 2) + heightLine ))
        }
        return path
    }
    
    override var intrinsicContentSize: CGSize {
        return CGSize(width: 120,
                      height: 250)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
