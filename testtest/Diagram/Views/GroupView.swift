//
//  GroupView.swift
//  testtest
//
//  Created by Francisco on 01/11/22.
//

import UIKit

class GroupView: UIView {
    let titleView = UIView()
    let contentView = UIView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    private func setupView() {
        backgroundColor = .green
        
    }
    
    override var intrinsicContentSize: CGSize {
        return CGSize(width: 80,
                      height: 260)
    }
    
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
