//
//  DiagramView.swift
//  testtest
//
//  Created by Francisco on 01/11/22.
//

import UIKit

class DiagramView: UIView {
    let groupAView = GroupView()
    let groupBView = GroupView()
    let groupCView = GroupView()
    let groupDView = GroupView()
    let groupEView = GroupView()
    let groupFView = GroupView()
    let groupGView = GroupView()
    let groupHView = GroupView()
        
    let firstEighthsView = MatchView()
    let secondEighthsView = MatchView()
    let thirdEighthsView = MatchView()
    let fourthEighthsView = MatchView()
    let fifthEighthsView = MatchView()
    let sixthEighthsView = MatchView()
    let seventhEighthsView = MatchView()
    let eighthEighthsView = MatchView()
    
    let firstQuarterView = MatchView()
    let secondQuarterView = MatchView()
    let thirdQuarterView = MatchView()
    let fourthQuarterView = MatchView()
    
    let firstSemiView = MatchView()
    let secondSemiView = MatchView()
    let finalView = MatchView()

    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .blue
        setupGroupsView()
        setupEightsView()
        setupQuartersView()
        setupSemisView()
        setupFinalView()
    }
    
    private func setupFinalView() {
        addSubview(finalView)
        finalView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            finalView.leadingAnchor.constraint(equalTo: firstSemiView.trailingAnchor,
                                                      constant: 50),
            finalView.centerYAnchor.constraint(equalTo: groupDView.bottomAnchor,
                                                      constant: 10)
        ])
    }
    
    private func setupSemisView() {
        addSubview(firstSemiView)
        firstSemiView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            firstSemiView.leadingAnchor.constraint(equalTo: firstQuarterView.trailingAnchor,
                                                      constant: 50),
            firstSemiView.centerYAnchor.constraint(equalTo: groupBView.bottomAnchor,
                                                      constant: 10)
        ])
        firstSemiView.createPath(heightLine: 450, direction: .bottom)
        
        addSubview(secondSemiView)
        secondSemiView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            secondSemiView.leadingAnchor.constraint(equalTo: firstQuarterView.trailingAnchor,
                                                      constant: 50),
            secondSemiView.centerYAnchor.constraint(equalTo: groupFView.bottomAnchor,
                                                      constant: 10)
        ])
        secondSemiView.createPath(heightLine: 450, direction: .top)
    }
    
    private func setupQuartersView() {
        addSubview(firstQuarterView)
        firstQuarterView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            firstQuarterView.leadingAnchor.constraint(equalTo: firstEighthsView.trailingAnchor,
                                                      constant: 50),
            firstQuarterView.centerYAnchor.constraint(equalTo: groupAView.bottomAnchor,
                                                      constant: 10)
        ])
        firstQuarterView.createPath(heightLine: 180, direction: .bottom)
        
        addSubview(secondQuarterView)
        secondQuarterView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            secondQuarterView.leadingAnchor.constraint(equalTo: firstEighthsView.trailingAnchor,
                                                      constant: 50),
            secondQuarterView.centerYAnchor.constraint(equalTo: groupCView.bottomAnchor,
                                                      constant: 10)
        ])
        secondQuarterView.createPath(heightLine: 180, direction: .top)
        
        addSubview(thirdQuarterView)
        thirdQuarterView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            thirdQuarterView.leadingAnchor.constraint(equalTo: firstEighthsView.trailingAnchor,
                                                      constant: 50),
            thirdQuarterView.centerYAnchor.constraint(equalTo: groupEView.bottomAnchor,
                                                      constant: 10)
        ])
        thirdQuarterView.createPath(heightLine: 180, direction: .bottom)
        
        addSubview(fourthQuarterView)
        fourthQuarterView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            fourthQuarterView.leadingAnchor.constraint(equalTo: firstEighthsView.trailingAnchor,
                                                      constant: 50),
            fourthQuarterView.centerYAnchor.constraint(equalTo: groupGView.bottomAnchor,
                                                      constant: 10)
        ])
        fourthQuarterView.createPath(heightLine: 180, direction: .top)
    }
    
    private func setupEightsView() {
        addSubview(firstEighthsView)
        firstEighthsView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            firstEighthsView.leadingAnchor.constraint(equalTo: groupAView.trailingAnchor,
                                                      constant: 50),
            firstEighthsView.centerYAnchor.constraint(equalTo: groupAView.centerYAnchor)
        ])
        firstEighthsView.createPath(heightLine: 50, direction: .bottom)

        
        addSubview(secondEighthsView)
        secondEighthsView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            secondEighthsView.leadingAnchor.constraint(equalTo: groupBView.trailingAnchor,
                                                      constant: 50),
            secondEighthsView.centerYAnchor.constraint(equalTo: groupBView.centerYAnchor)
        ])
        secondEighthsView.createPath(heightLine: 50, direction: .top)

        
        addSubview(thirdEighthsView)
        thirdEighthsView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            thirdEighthsView.leadingAnchor.constraint(equalTo: groupCView.trailingAnchor,
                                                      constant: 50),
            thirdEighthsView.centerYAnchor.constraint(equalTo: groupCView.centerYAnchor)
        ])
        thirdEighthsView.createPath(heightLine: 50, direction: .bottom)
        
        addSubview(fourthEighthsView)
        fourthEighthsView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            fourthEighthsView.leadingAnchor.constraint(equalTo: groupDView.trailingAnchor,
                                                      constant: 50),
            fourthEighthsView.centerYAnchor.constraint(equalTo: groupDView.centerYAnchor)
        ])
        fourthEighthsView.createPath(heightLine: 50, direction: .top)
        
        addSubview(fifthEighthsView)
        fifthEighthsView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            fifthEighthsView.leadingAnchor.constraint(equalTo: groupEView.trailingAnchor,
                                                      constant: 50),
            fifthEighthsView.centerYAnchor.constraint(equalTo: groupEView.centerYAnchor)
        ])
        fifthEighthsView.createPath(heightLine: 50, direction: .bottom)
        
        addSubview(sixthEighthsView)
        sixthEighthsView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            sixthEighthsView.leadingAnchor.constraint(equalTo: groupFView.trailingAnchor,
                                                      constant: 50),
            sixthEighthsView.centerYAnchor.constraint(equalTo: groupFView.centerYAnchor)
        ])
        sixthEighthsView.createPath(heightLine: 50, direction: .top)
        
        addSubview(seventhEighthsView)
        seventhEighthsView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            seventhEighthsView.leadingAnchor.constraint(equalTo: groupGView.trailingAnchor,
                                                      constant: 50),
            seventhEighthsView.centerYAnchor.constraint(equalTo: groupGView.centerYAnchor)
        ])
        seventhEighthsView.createPath(heightLine: 50, direction: .bottom)
        
        addSubview(eighthEighthsView)
        eighthEighthsView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            eighthEighthsView.leadingAnchor.constraint(equalTo: groupHView.trailingAnchor,
                                                      constant: 50),
            eighthEighthsView.centerYAnchor.constraint(equalTo: groupHView.centerYAnchor)
        ])
        eighthEighthsView.createPath(heightLine: 50, direction: .top)
    }
    
    private func setupGroupsView() {
        addSubview(groupAView)
        groupAView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            groupAView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 12),
            groupAView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 12),
        ])
        
        addSubview(groupBView)
        groupBView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            groupBView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 12),
            groupBView.topAnchor.constraint(equalTo: groupAView.bottomAnchor, constant: 20),
        ])
        
        addSubview(groupCView)
        groupCView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            groupCView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 12),
            groupCView.topAnchor.constraint(equalTo: groupBView.bottomAnchor, constant: 20),
        ])
        
        addSubview(groupDView)
        groupDView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            groupDView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 12),
            groupDView.topAnchor.constraint(equalTo: groupCView.bottomAnchor, constant: 20),
        ])
        
        addSubview(groupEView)
        groupEView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            groupEView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 12),
            groupEView.topAnchor.constraint(equalTo: groupDView.bottomAnchor, constant: 20),
        ])
        
        addSubview(groupFView)
        groupFView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            groupFView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 12),
            groupFView.topAnchor.constraint(equalTo: groupEView.bottomAnchor, constant: 20),
        ])
     
        addSubview(groupGView)
        groupGView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            groupGView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 12),
            groupGView.topAnchor.constraint(equalTo: groupFView.bottomAnchor, constant: 20),
        ])
        
        addSubview(groupHView)
        groupHView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            groupHView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 12),
            groupHView.topAnchor.constraint(equalTo: groupGView.bottomAnchor, constant: 20),
        ])
    }
    
    override var intrinsicContentSize: CGSize {
        return CGSize(width: 810, height: 2250)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
